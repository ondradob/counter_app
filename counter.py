from flask import Flask
import psycopg2
import redis
from datetime import datetime

app = Flask(__name__)

conn_postgres = psycopg2.connect(
    host="postgres",
    database="database",
    user="ondra",
    password="ondra"
)
cur_postgres = conn_postgres.cursor()

redis_client = redis.Redis(host='redis', port=6379, decode_responses=True)

cur_postgres.execute("""
    CREATE TABLE IF NOT EXISTS visits (
        id SERIAL PRIMARY KEY,
        visit_timestamp TIMESTAMP
    );
""")
conn_postgres.commit()

@app.route('/')
def index():
    current_time = datetime.now()
    cur_postgres.execute("""
        INSERT INTO visits (visit_timestamp) VALUES (%s);
    """, (current_time,))
    conn_postgres.commit()

    redis_client.incr('counter')

    counter_value = redis_client.get('counter')

    html_content = f"""
    <html>
    <head><title>Counter App</title></head>
    <body>
    <h1>Welcome!</h1>
    <p>Number of visits: {counter_value}</p>
    <p>Last visit: {current_time}</p>
    </body>
    </html>
    """

    return html_content

if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000)
